package figuras;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.TexturePaint;
import java.awt.geom.Arc2D;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import javax.swing.JFrame;
public class Figuras extends JFrame{
    public Figuras ()
    {
        super("Figuras en 2D");
        setSize(425, 1600);
        setVisible(true);
    }
    //Figuras con la APD Java2D
    public void paint(Graphics g)
    {
        super.paint(g); //llamar al metodo paint de la superclase
        Graphics2D g2d = (Graphics2D) g; //cast, convertir g a Graphics2D
        //elipse 2D rellena con degradado azul-amarillo
        g2d.setPaint(new GradientPaint(5, 30, Color.BLUE, 35, 100, Color.YELLOW, true));
        g2d.fill(new Rectangle2D.Double(80, 30, 65, 100));
        //Rectangulo 2D en rojo
        g2d.setPaint(Color.RED);
        g2d.setStroke(new BasicStroke(10.0f));
        g2d.draw(new Rectangle2D.Double(80, 30, 65, 100));
        //Rectangulo 2D redondeado con fondo texturizado
        BufferedImage buffImage = new BufferedImage(10, 10, BufferedImage.TYPE_INT_RGB);
        Graphics2D gg = buffImage.createGraphics();
        gg.setColor(Color.YELLOW);
        gg.fillRect(0, 0, 10, 10);
        gg.setColor(Color.BLACK);
        gg.drawRect(6, 6, 6, 6);
        gg.setColor(Color.WHITE);
        gg.fillRect(1, 1, 3, 3);
        gg.setColor(Color.RED);
        gg.fillRect(6, 6, 6, 6);
        //pintar bufferImage en el objeto JFrame
        g2d.setPaint(new TexturePaint(buffImage, new Rectangle(10, 10)));
        //Pintar buffer
        g2d.setPaint(java.awt.Color.WHITE);
        g2d.setStroke(new BasicStroke(6.0f));
        g2d.draw(new Arc2D.Double(240, 30, 75, 100, 0, 270, Arc2D.PIE));
        //Lineas 2D en verde y amarillo
        g2d.setPaint(Color.GREEN);
        g2d.draw(new Line2D.Double(395, 30, 320, 150));
        float guiones [] = {10};
        g2d.setPaint(Color.YELLOW);
        g2d.setStroke(new BasicStroke(4, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND, 10, guiones, 0));
        g2d.draw(new Line2D.Double(320, 30, 395, 150));
    }//fin del metodo paint
    public static void main(String [] args)
    {
        Figuras aplicacion = new Figuras();
        aplicacion.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}